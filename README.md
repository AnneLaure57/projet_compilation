# Projet Compilation

Projet compilation - L3 MIASHS

Nous avons commencé par la établir la grammaire de notre jeu en en JFlex, puis au fur et à mesure nous avons crée les règles de production. Nous avons fonctionné sur le principe que celui vu en TD (sous forme d'arborescence). 
Nous avons donc les tests numéro 1 à 8 qui sont fonctionnels et le test numéro 9 qui nous renvoie une erreur de syntaxe.

## TODO

## Technologies utilisées

Pour ce projet, nous avons travaillé pour le moment avec les technologies cup et JFlex.
Logiciel utilisé : Eclipse
Répertoire : Bitbucket

##Commandes utiles pour maven 

projet-compilation generate-sources
projet-compilation package

##Pour éxécuter le code sur Eclipse

Lancer le main et mettez en paramètre, un des fichiers de notre jeu de tests

```
maven generate-sources (dans le cas ou le parser.cup ou le fichier .lex a été modifié depuis le dernier lancé)
maven package
run configurations -> main 
args -> tests/nomdufichier.txt

```

##Améliorations effectuées

à voir pendant la soutenance le 23/04.

## Requis
JLex, Cup, Eclipse, Api Arbre

## Auteurs

BAZART Médérice - CHARLES Anne-Laure - DAUBENFELD Gabriel - DESMET Quentin - KRATZ Juliette

## Licence

Ce projet est distribué sous une licence MIT
(voir [license.md](license.md) pour plus de détails)