/* Scanner pour le projet Compilation */
package generated.fr.ul.miashs.projet_compilation;
import java_cup.runtime.Symbol;
 
%%
/* options */

%line
%public
%cupsym Sym
%cup
%debug
 
 
/* macros */

finLigne = \r|\n|\r\n 
WhiteSpace     = {finLigne} | [ \t\f]
const = [-]?[0-9]+
id = [A-Za-z_][A-Za-z_0-9]*
c1 = \/\/.*|\?[^]|\\
c2 = \/\*(([^*])|(\*[^/]))*\*\/
comment = {c1}|{c2}
 
%%
/* ------------------------Règles lexicales---------------------- */
   
/* Return the token SEMI declared in the class Sym that was found. */
    ";"                { return (new Symbol(Sym.SEMI)); }
    ","                { return (new Symbol(Sym.COMMA)); }

/*Opérateurs arithmétiques*/
    "+"                { return (new Symbol(Sym.ADD)); }
    "-"                { return (new Symbol(Sym.SUB)); }
    "*"                { return (new Symbol(Sym.MUL)); }
    "/"                { return (new Symbol(Sym.DIV)); }
    
  
/*Balises*/
    "("                { return (new Symbol(Sym.PO)); }
    ")"                { return (new Symbol(Sym.PF)); }
    "="                { return (new Symbol(Sym.EQ)); }
    "{"                { return (new Symbol(Sym.AO)); }
    "}"                { return (new Symbol(Sym.AF)); }

/*Expressions conditionnelles*/
    "if"               { return (new Symbol(Sym.IF)); }
    "else"             { return (new Symbol(Sym.ELSE));  }
    "while"            { return (new Symbol(Sym.WHILE)); }
    
/*Types*/
    "int"              { return (new Symbol(Sym.INT));}

/*Opérateurs logiques*/
    "<"                { return (new Symbol(Sym.LT));}
    ">"                { return (new Symbol(Sym.GT));}
    ">="               { return (new Symbol(Sym.GTE));}
    "<="               { return (new Symbol(Sym.LTE));}
    "=="               { return (new Symbol(Sym.ISEQ));}
    "!="            {return new Symbol(Sym.ISDIFF);}
    
/*Types de fonctions*/
    "void"             { return (new Symbol(Sym.VOID));}
    "printl"		   { return (new Symbol(Sym.PR));}
    "return"           { return (new Symbol(Sym.RETURN));}

/*Litterales*/
    {const}           {return (new Symbol(Sym.CONST, new Integer(yytext())));}
    {id}       		  {return (new Symbol(Sym.ID, yytext()));}
        
/* Ne fais rien si whitespace est trouvé */
    {WhiteSpace}       { /* espace */;} 
    {comment}          {/*Ceci est un commentaire*/;}