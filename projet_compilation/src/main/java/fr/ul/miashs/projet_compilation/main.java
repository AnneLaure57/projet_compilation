package fr.ul.miashs.projet_compilation;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Reader;

import fr.ul.miashs.api.arbre.Afficheur;
import generated.fr.ul.miashs.projet_compilation.ParserCup;
import generated.fr.ul.miashs.projet_compilation.Yylex;



public class main { 

	public static void main(String[] args) {
		
		try {
			String filename="";
				filename="test_2.txt";
				Reader reader = new FileReader("tests/"+filename);
				Yylex scanner = new Yylex(reader);
				ParserCup parser = new ParserCup(scanner);
				parser.parse();
				System.out.println("La lecture est terminée ! nom fichier lu : "+filename);	
				
				Afficheur.afficher(parser.resultat);
				
				System.out.println("TDS"+parser.TDS);
				
				String filenameOut="assembleur_"+filename+".uasm";
				FileWriter fw = new FileWriter(filenameOut);
				PrintWriter pw = new PrintWriter(fw);
				pw.print(parser.uasm.getCode());
				pw.close();
		} catch (Exception e) {
			System.err.println("Erreur de syntaxe");
			e.printStackTrace();
			System.exit(0);
		}
	}

}