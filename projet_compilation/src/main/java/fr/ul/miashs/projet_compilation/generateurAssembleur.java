package fr.ul.miashs.projet_compilation;

import fr.ul.miashs.TDS.Symbole;
import fr.ul.miashs.TDS.SymboleVar;
import fr.ul.miashs.TDS.TableDesSymboles;
import fr.ul.miashs.api.arbre.*;


public class generateurAssembleur {

	private TableDesSymboles tds;
	private Noeud noeud;
	private String code;
	private String scope;

	public generateurAssembleur(TableDesSymboles tds, Noeud noeud, String scope) {
		super();
		this.tds = tds;
		this.noeud = noeud;
		this.code = "";
		this.setScope(scope);
	}

	public String genererCode() {
		genererProg(this.noeud);
		return this.code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	private void genererProg(Noeud noeud) {
		this.code += ".include beta.uasm\n.include intio.uasm\n.options tty\n\tCMOVE(pile, SP)\n\tBR(debut)";
		genererVariableGlobal();
		for (Noeud n : this.noeud.getFils()) {
			genererFonction(n);
		}
		this.code += "\ndebut:\n\tCALL(main)\n\tHALT()";
		this.code += "\npile :";
	}

	private void genererVariable(String scope) {
		for (Symbole sym : tds.getTableDesSymboles()) {
			if (sym.getScope() == scope && sym.getGenre() == "Variable") {
				this.code += "\n" + sym.getNom() + ":\tLONG(" + ((SymboleVar) sym).getValeur() + ")";
			}
		}
	}

	private void genererVariableGlobal() {
		genererVariable("Global");
	}

	private void genererVariableLocal(String scope) {
		genererVariable(scope);
	}

	private void genererFonction(Noeud n) {
		// this.setScope("Global");
		this.code += "\n" + ((Fonction) n).getValeur().toString() + ":";
		genererVariableLocal(((Fonction) n).getValeur().toString());
		this.code += "\n\tPUSH(LP)\n\tPUSH(BP)\n\tMOVE(SP,BP)\n\tALLOCATE(" + n.getFils().size() + ")";
		genererBloc(n.getFils().get(0));
		this.code += "\nretour_" + ((Fonction) n).getValeur().toString() + ":";
		this.code += "\n\tDEALLOCATE(" + n.getFils().size() + ")\n\tPOP(BP)\n\tPOP(LP)\n\tRTN()";
	}

	private void genererBloc(Noeud n) {
		for (Noeud noeud : n.getFils()) {
			genererInstruction(noeud);
		}
	}

	private void genererInstruction(Noeud n) {
		switch (n.getCat()) {
		case AFF:
			genererAffectation(n);
			break;
		case ECR:
			genererEcrire();
			break;
		case APPEL:
			genererAppel(n);
			break;
		case RET:
			genererRetour(n);
			break;
		case SI:
			genererSi(n);
			break;
		case TQ:
			//genererTq(n);
			break;
		default:
			break;

		}
	}

	private void genererAffectation(Noeud n) {
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R0)\n\tST(R0," + ((Idf) ((Affectation) n).getFilsGauche()).getValeur() + ")";
	}

	private void genererRetour(Noeud n) {
		for (Noeud no : n.getFils()) {
			genererExpression(no);
		}
		// this.code +=
		// "\n\tPOP(RO)\n\toffset=(1+4)*-4\n\tPUTFRAME(R0,offset)\n\tBR(retour_"+(Idf)n.g
	}

	private void genererSi(Noeud n){
		this.code +="\n si #"+((Idf)n).getValeur().toString()+":";
		genererCond(n.getFils().get(0));
		this.code +="\n\tPOP(R0)\n\tBF(R0,sinon #"+((Idf)n).getValeur().toString()+")";
		this.code +="\n alors #"+((Idf)n).getValeur().toString()+":";
		genererBloc(n.getFils().get(1));
		this.code+="\n\tBR(fsi#"+((Idf)n).getValeur().toString()+")";
		genererBloc(n.getFils().get(2));
		this.code+="\n fsi #"+((Idf)n).getValeur().toString()+":";
	}

	private void genererCond(Noeud n) {
		switch (n.getCat()) {
		case EG:
			genererEg(n);
			break;
		case INF:
			genererInf(n);
			break;
		case INFE:
			genererInfe(n);
			break;
		case SUP:
			genererSup(n);
			break;
		case SUPE:
			genererSupe(n);
			break;
		default:
			break;

		}
	}

	private void genererEg(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tCMPEQ(R2,R1,R0)\n\tPUSH(R0)";
	}

	private void genererSup(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tCMPLE(R2,R1,R0)\n\tPUSH(R0)";
	}

	private void genererSupe(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tCMPL(R2,R1,R0)\n\tPUSH(R0)";
	}

	private void genererInf(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tCMPL(R1,R2,R0)\n\tPUSH(R0)";
	}

	private void genererInfe(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tCMPL(R1,R2,R0)\n\tPUSH(R0)";
	}

	private void genererExpression(Noeud n) {
		switch (n.getCat()) {
		case CONST:
			genererConst(n);
			break;
		case IDF:
			genererIdf(n);
			break;
		case LIRE:
			genererLire();
			break;
		case MOINS:
			genererMoins(n);
			break;
		case MUL:
			genererMul(n);
			break;
		case PLUS:
			genererPlus(n);
			break;
		case DIV:
			genererDiv(n);
			break;
		case APPEL:
			genererAppel(n);
			break;
		default:
			break;

		}
	}

	private void genererPlus(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tADD(R1,R2,R0)\n\tPUSH(R0)";
	}

	private void genererMoins(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tSUB(R1,R2,R0)\n\tPUSH(R0)";
	}

	private void genererMul(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tMUL(R1,R2,R0)\n\tPUSH(R0)";
	}

	private void genererDiv(Noeud n) {
		genererExpression(((Affectation) n).getFilsGauche());
		genererExpression(((Affectation) n).getFilsDroit());
		this.code += "\n\tPOP(R2)\n\tPOP(R1)\n\tDIV(R1,R2,R0)\n\tPUSH(R0)";
	}

	private void genererLire() {
		this.code += "\n\tRDINT()";
	}

	private void genererEcrire() {
		this.code += "\n\tWRINT()";
	}

	private void genererAppel(Noeud n) {
		this.code += "\n\tALLOCATE(1)";
		for (Noeud no : n.getFils()) {
			genererExpression(no);
		}
		this.code += "\n\tCALL(" + ((Idf) n).getLabel().toString() + ")";
	}

	private void genererConst(Noeud n) {
		this.code += "\n\tCMOVE(" + ((Const) n).getValeur() + ",R0)\n\tPUSH(R0)";
	}

	private void genererIdf(Noeud n) {
		if (this.tds.chercherSymboleBis(((Idf) noeud).getValeur().toString(), "global").getGenre() != "Parametre") {
			this.code += "\n\tLD("
					+ ((SymboleVar) this.tds.chercherSymboleBis(((Idf) noeud).getValeur().toString(), "global"))
							.getValeur()
					+ ",R0)\n\tPUSH(R0)";
		} else {
			this.code += "\n\tGETFRAME("
					+ ((SymboleVar) this.tds.chercherSymboleBis(((Idf) noeud).getValeur().toString(), scope.toString()))
							.getValeur()
					+ ",R0)\n\tPUSH(R0)";
		}
	}

	public TableDesSymboles getTds() {
		return tds;
	}

	public void setTds(TableDesSymboles tds) {
		this.tds = tds;
	}

	public Noeud getNoeud() {
		return noeud;
	}

	public void setNoeud(Noeud noeud) {
		this.noeud = noeud;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
}
