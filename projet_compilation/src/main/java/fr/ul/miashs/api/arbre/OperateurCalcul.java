package fr.ul.miashs.api.arbre;

public class OperateurCalcul extends Noeud0 {
	// constructeur
	protected OperateurCalcul() {
	}

	// methodes
	/**
	 * @return le fils gauche (par convention à l'indice 0)
	 */
	public Noeud getFilsGauche() {
		return getFils().get(0);
	}

	/**
	 * @return le fils droit (par convention à l'indice 1)
	 */
	public Noeud getFilsDroit() {
		return getFils().get(1);
	}

	/**
	 * Ajoute un fils gauche
	 */
	public void setFilsGauche(Noeud n) {
		getFils().add(0, n);
	}

	/**
	 * Ajoute un fils droit
	 */
	public void setFilsDroit(Noeud n) {
		getFils().add(1, n);
	}
}
