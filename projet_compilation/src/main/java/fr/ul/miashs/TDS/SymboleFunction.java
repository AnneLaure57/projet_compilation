package fr.ul.miashs.TDS;

public class SymboleFunction extends Symbole{

	private int nbValLocal;
	private int nbParam;
	
	
	public SymboleFunction(String nom, String type, int nbValLocal, int nbParam) {
		super(nom, type, "Global", "Fonction");
		this.nbValLocal = nbValLocal;
		this.nbParam = nbParam;
	}
	
	
	public int getNbValLocal() {
		return nbValLocal;
	}
	public void setNbValLocal(int nbValLocal) {
		this.nbValLocal = nbValLocal;
	}
	public int getNbParam() {
		return nbParam;
	}
	public void setNbParam(int nbParam) {
		this.nbParam = nbParam;
	}


	@Override
	public String toString() {
		return "SymboleFunction [nbValLocal=" + nbValLocal + ", nbParam=" + nbParam + ", nom=" + nom + ", type=" + type
				+ ", scope=" + scope + ", genre=" + genre + "]";
	}
}
