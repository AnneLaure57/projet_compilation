package fr.ul.miashs.TDS;

public class SymboleVar extends Symbole{

	private int valeur;

	public SymboleVar(String nom, String scope, int valeur) {
		super(nom, "entier", scope, "Variable");
		this.valeur = valeur;
	}

	public int getValeur() {
		return valeur;
	}

	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	@Override
	public String toString() {
		return "SymboleVar [valeur=" + valeur + ", nom=" + nom + ", type=" + type + ", scope=" + scope + ", genre="
				+ genre + "]";
	}
	
}
