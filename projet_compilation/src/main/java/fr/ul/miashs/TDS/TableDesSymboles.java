package fr.ul.miashs.TDS;

import java.util.ArrayList;

public class TableDesSymboles {

	private ArrayList<Symbole> tableDesSymboles;

	public TableDesSymboles() {
		this.tableDesSymboles = new ArrayList<Symbole>();
	}

	public ArrayList<Symbole> getTableDesSymboles() {
		return tableDesSymboles;
	}

	public void setTableDesSymboles(ArrayList<Symbole> tableDesSymboles) {
		this.tableDesSymboles = tableDesSymboles;
	}

	public boolean chercherSymbole(Symbole sym) {
		for (Symbole symbole : this.tableDesSymboles) {
			if (symbole.getNom().equals(sym.getNom())
					&& (symbole.getScope().equals(sym.getScope()) || symbole.getScope().equals("Global"))) {
				return true;
			}
		}
		return false;
	}

	public void ajoutSymbole(Symbole sym) throws Exception {
		if (!chercherSymbole(sym)) {
			this.tableDesSymboles.add(sym);
		}
//			else {
//				Symbole symbole = chercherSymbole(sym);
//				if(symbole.getScope().equals(sym.getScope())) {
//					throw new Exception("Double définition de la variable "+sym.getNom());
//				}
//			}
	}

	public Symbole trouverSymbole(String var, String idf) {
		for (Symbole symbole : this.tableDesSymboles) {
			if (symbole.getNom().equals(var) && symbole.getScope().equals(idf))
				return symbole;
		}
		return null;
	}

	public Symbole chercherSymboleBis(String nom, String scope) {
		for (Symbole symbole : this.tableDesSymboles) {
			if (symbole.getNom().equals(nom)
					&& (symbole.getScope().equals(scope) || symbole.getScope().equals("Global"))) {
				return symbole;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		String res = "";
		for (Symbole s : tableDesSymboles)
			res += s + "\n";
		return "TableDesSymboles:\n" + res;
	}
}
