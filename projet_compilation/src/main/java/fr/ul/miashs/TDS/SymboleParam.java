package fr.ul.miashs.TDS;

public class SymboleParam extends Symbole{

	private int rang;

	public SymboleParam(String nom, String scope, int rang) {
		super(nom, "entier", scope, "Variable");
		this.rang = rang;
	}

	public int getRang() {
		return rang;
	}

	public void setRang(int rang) {
		this.rang = rang;
	}

	@Override
	public String toString() {
		return "SymboleParam [rang=" + rang + ", nom=" + nom + ", type=" + type + ", scope=" + scope + ", genre="
				+ genre + "]";
	}
}
