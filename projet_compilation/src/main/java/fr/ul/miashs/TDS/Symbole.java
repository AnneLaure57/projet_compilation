package fr.ul.miashs.TDS;

public abstract class Symbole {

	protected String nom;
	protected String type;
	protected String scope;
	protected String genre;
	
	
	public Symbole(String nom, String type, String scope, String genre) {
		super();
		this.nom = nom;
		this.type = type;
		this.scope = scope;
		this.genre = genre;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getScope() {
		return scope;
	}


	public void setScope(String scope) {
		this.scope = scope;
	}


	public String getGenre() {
		return genre;
	}


	public void setGenre(String genre) {
		this.genre = genre;
	}

}
